<!DOCTYPE html>
<?php
  session_start();
 ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Magebit-test</title>
    <link type="text/css" rel="stylesheet" href="normalize.css" />
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>

    <div class="signup-form">
        <div class="signup-blue">

            <h2>Don’t have an account?</h2><br>
            <hr><br>
            <h5>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit,
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5><br>
            <button id="button-register">SIGN UP</button>

        </div>
        <div class="signup-blue2">
            <h2>Have an account?</h2><br>
            <hr><br>
            <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h5><br>
            <button id="button-login">LOGIN</button>
          </div>
        </div>
        <img id="white-square" src="img/white-square.png">

    <div class="signup-white">
      <form action="php/signup.inc.php" method="post">
        <h1>Sign Up</h1>
        <img id="logo" src="img/logo.png">
        <hr><br>
        <img id='user_non' src="img/ic_user_non.png">
        <img id='user_logo' src="img/ic_user.png">
        <h4>Name<span style="color: #ff0000">*</span></h4>
        <input id="user" type="text" name="first">
        <br>
        <img id='user_email_non' src="img/ic_mail_non.png">
        <img id='user_email' src="img/ic_mail.png">
        <h4>Email<span style="color: #ff0000">*</span></h4>
        <input id="email" type="text" name="email"><br>
        <img id='user_pwd_non' src="img/ic_lock_non.png">
        <img id='user_pwd' src="img/ic_lock.png">
        <h4>Password<span style="color: #ff0000">*</span></h4>
        <input id="pwd" type="password" name="pwd"><br>
        <input id="button-sign" type="submit" name="register" value="SIGN UP">
        </form>
      </div>
      <div class="login-white">
        <form action="php/login.inc.php" method="post">
          <h1>Login</h1>
          <img id="logo1" src="img/logo.png">
          <hr><br>
          <img id='user_email_non1' src="img/ic_mail_non.png">
          <img id='user_email1' src="img/ic_mail.png">
          <h4>Email<span style="color: #ff0000">*</span></h4>
          <input id="email-log" type="text" name="email2"><br>
          <img id='user_pwd_non1' src="img/ic_lock_non.png">
          <img id='user_pwd1' src="img/ic_lock.png">
          <h4>Password<span style="color: #ff0000">*</span></h4>
          <input id="pwd-log" type="password" name="pwd2"><br>
          <a href="#">Forgot?</a>
          <input id="button-login1" type="submit" name="login" value="LOGIN">
          </form>
        </div>
    
    <footer>
      All Rights Reserved “Magebit” 2016.
    </footer>
  </body>

</html>
