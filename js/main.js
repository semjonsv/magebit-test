$(document).ready(function(){
  if (document.documentElement.clientWidth < 1185) {
    $(window).resize(function(){location.reload();});
    $("#button-login").click(function(){
      $(".signup-white").animate({
        top: '550px',
        opacity: '0',
      });
      $("#white-square").animate({
        top: '550px',
      });
      $(".login-white").show();
      $(".login-white").animate({
        opacity: '1',
        top: '550px',
      });
      });

    $("#button-register").click(function(){
      $(".login-white").hide();
      $(".login-white").animate({
        top: '140px',
        opacity: '0',
      });
      $("#white-square").animate({
      top: '140px',
      });
      $(".signup-white").animate({
        top: '140px',
        opacity: '1',
      });
      });
  } else {
    $(window).resize(function(){location.reload();});
    $("#button-login").click(function(){
      $(".signup-white").animate({
        left: '700px',
        opacity: '0',
      });
      $("#white-square").animate({
        left: '700px',
      });
      $(".login-white").show();
      $(".login-white").animate({
        opacity: '1',
        left: '700px',
      });
      });

    $("#button-register").click(function(){
      $(".login-white").hide();
      $(".login-white").animate({
        left: '260px',
        opacity: '0',
      });
      $("#white-square").animate({
      left: '260px',
      });
      $(".signup-white").animate({
        left: '260px',
        opacity: '1',
      });
      });
  }
  $("#user").keyup(function () {
     if ($(this).val()) {
        $("#user_logo").show();
        $("#user_non").hide();
     }
     else {
        $("#user_logo").hide();
        $("#user_non").show();
     }
  });

  $("#email").keyup(function () {
     if ($(this).val()) {
        $("#user_email").show();
        $("#user_email_non").hide();
     }
     else {
        $("#user_email").hide();
        $("#user_email_non").show();
     }
  });

  $("#pwd").keyup(function () {
     if ($(this).val()) {
        $("#user_pwd").show();
        $("#user_pwd_non").hide();
     }
     else {
        $("#user_pwd").hide();
        $("#user_pwd_non").show();
     }
  });



    $("#email-log").keyup(function () {
       if ($(this).val()) {
          $("#user_email1").show();
          $("#user_email_non1").hide();
       }
       else {
          $("#user_email1").hide();
          $("#user_email_non1").show();
       }
    });

    $("#pwd-log").keyup(function () {
       if ($(this).val()) {
          $("#user_pwd1").show();
          $("#user_pwd_non1").hide();
       }
       else {
          $("#user_pwd1").hide();
          $("#user_pwd_non1").show();
       }
    });


});
