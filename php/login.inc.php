<?php
session_start();
include_once 'register.inc.php';
class Login extends User{
  function LoginAsUser(){
    if(isset($_POST['login'])){
      $this->email = mysqli_real_escape_string($this->connect(), $_POST['email2']);
      $this->password = mysqli_real_escape_string($this->connect(), $_POST['pwd2']);
      if (empty($this->email) || empty($this->password)) {
        header("Location: ../index.php?empty-login-fields");
        exit();
      }else {
        $sql = "SELECT * FROM users WHERE user_email='$this->email'";
        $result = mysqli_query($this->connect(),$sql);
        $count = mysqli_num_rows($result);
        if ($count > 0) {
          $row = mysqli_fetch_assoc($result);
          if (!password_verify($this->password, $row['user_pwd'])) {
            header("Location: ../index.php?wrong-login-pwd");
            exit();
            } else {
              $_SESSION['user_first'] = $row['user_first'];
              $_SESSION['user_email'] = $row['user_email'];
              $_SESSION['user_token'] = $row['user_token'];
              header("Location: ../index.php?logged");
              exit();
              }
          }else {
            header("Location: ../index.php?no-such-email");
            exit();
          }
          }
    }else {
      header("Location: ../index.php");
      exit();
    }
  }
}
$new = new Login;
$new->LoginAsUser();
