<?php
include_once 'dbh.inc.php';

class User extends Dbh{
  protected $email;
  protected $username;
  protected $password;
  protected $token;
  protected $hashedPwd;


  public function Validate(){

  if(isset($_POST['register'])){
    $this->username = mysqli_real_escape_string($this->connect(), $_POST['first']);
    $this->password = mysqli_real_escape_string($this->connect(), $_POST['pwd']);
    $this->email = mysqli_real_escape_string($this->connect(), $_POST['email']);

    if (empty($this->username) || empty($this->password) ||  empty($this->email)) {
      header("Location: ../index.php?empty-fields");
      exit();
    } else {
      if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
        header("Location: ../index.php?not-email");
        exit();
      } else {
          $sql_u = "SELECT * FROM users WHERE user_first='$this->username'";
  	      $res_u = mysqli_query($this->connect(), $sql_u);
          $count = mysqli_num_rows($res_u);
          if ($count> 0 ) {
            header("Location: ../index.php?username=taken");
            exit();
          }else {
            $sql_e = "SELECT * FROM users WHERE user_email='$this->email'";
            $res_e = mysqli_query($this->connect(), $sql_e);
            if (mysqli_num_rows($res_e) > 0 ) {
              header("Location: ../index.php?email=taken");
              exit();
            } else {
              $this->hashedPwd = password_hash($this->password, PASSWORD_DEFAULT);
              $this->token = bin2hex(random_bytes(16));
              $sql = "INSERT INTO users (user_first, user_email, user_pwd, user_token)
              VALUES ('$this->username', '$this->email', '$this->hashedPwd', '$this->token')";
              mysqli_query($this->connect(), $sql);
              $sql = "INSERT INTO users_info (user_first, user_token)
              VALUES ('$this->username', '$this->token')";
              mysqli_query($this->connect(), $sql);
              header("Location: ../index.php?succes");
              exit();
            }
          }
        }
      }
    }

  else {
    header("Location: ../index.php");
    exit();
  }
  }
}
